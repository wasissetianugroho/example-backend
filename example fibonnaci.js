function fibonnaci(n) {
    if (n === 0) return [];
    if (n === 1) return [0];
    if (n === 2) return [0, 1];
    const sequence = [0,1]
    for(let i = 2; i < n; i++) {
      const nextValue = sequence[i -1] + sequence[i -2]
      sequence.push(nextValue)
    }
    return sequence
  }
  const n = 9
  const fibonnaciSequence = fibonnaci(n)
  console.log(fibonnaciSequence)



