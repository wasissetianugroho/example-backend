// function ispalindrome(str) {
//     return str === str.split("").reverse().join("")
// }

// console.log(ispalindrome("kepala kambing"))

function isPalindrom(str) {
    str = str.replace(/[^\w]/g, "").toLowerCase()
    const reversed = str.split("").reverse().join("");
    return str === reversed;
  }
  const input1 = "kasur rusak"
  console.log(isPalindrom(input1))