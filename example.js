function addNumbers(num1, num2) {
    return num1 + num2
    }
    
    const firstNumber = 5
    const secondNumber = 10
    
    const sum = addNumbers(firstNumber, secondNumber)
    console.log(`The sum of ${firstNumber} and ${secondNumber} is: ${sum}`)
    
    function concatenateStrings(str1, str2) {
        return str1 + " " + str2
    }
    
    const firstName = "John"
    const lastName = "Doe"
    
    const fullName = concatenateStrings(firstName, lastName)
    console.log(`Full name: ${fullName}`)
    
    function findmaximum(num1, num2)  {
        return Math.max(num1, num2)
    }
    const number1 = 7
    const number2 = 12
    const maxNumber = findmaximum(number1, number2)
    
    console.log(`The maximum of ${number1} and ${number2} is: ${maxNumber}`)

    function bubbleSort(arr) {
        let swapped;
        do {
          swapped = false;
          for (let i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
              [arr[i], arr[i + 1]] = [arr[i + 1], arr[i]];
              swapped = true;
            }
          }
        } while (swapped);
        return arr;
      }
      
      const array = [64, 34, 25, 12, 22, 11, 90];
      console.log("Bubble Sort:", bubbleSort(array));

      
      
    